/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis.client.commands.impl

public class SentinelCommandsBuilderImpl <: SentinelCommandsBuilder {
    public func sentinelFailover(masterName: String): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(SentinelKeyWord.FAILOVER).add(masterName)
        return ParameterizedRedisCommand<String>(
            CommandType.SENTINEL,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
    }

    public func sentinelGetMasterAddrByName(masterName: String): ParameterizedRedisCommand<?(String, String)> {
        let commandArgs = CommandArgs().add(SentinelKeyWord.GET_MASTER_ADDR_BY_NAME).add(masterName)
        return ParameterizedRedisCommand<?(String, String)>(
            CommandType.SENTINEL,
            commandArgs,
            ResponseBuilderFactory.nillableStringTupleBuilder
        )
    }

    public func sentinelMaster(masterName: String): ParameterizedRedisCommand<HashMap<String, String>> {
        let commandArgs = CommandArgs().add(SentinelKeyWord.MASTER).add(masterName)
        return ParameterizedRedisCommand<HashMap<String, String>>(
            CommandType.SENTINEL,
            commandArgs,
            ResponseBuilderFactory.stringToStringMapBuilder
        )
    }

    public func sentinelMasters(): ParameterizedRedisCommand<ArrayList<HashMap<String, String>>> {
        let commandArgs = CommandArgs().add(SentinelKeyWord.MASTERS)
        return ParameterizedRedisCommand<ArrayList<HashMap<String, String>>>(
            CommandType.SENTINEL,
            commandArgs,
            ListOfMapResponseBuilder.INSTANCE
        )
    }

    public func sentinelMonitor(masterName: String, ip: String, port: Int64, quorum: Int64): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(SentinelKeyWord.MONITOR).add(masterName).add(ip).add(port).add(quorum)
        return ParameterizedRedisCommand<String>(
            CommandType.SENTINEL,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
    }

    public func sentinelMyId(): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(SentinelKeyWord.MYID)
        return ParameterizedRedisCommand<String>(
            CommandType.SENTINEL,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
    }

    public func sentinelRemove(masterName: String): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(SentinelKeyWord.REMOVE).add(masterName)
        return ParameterizedRedisCommand<String>(
            CommandType.SENTINEL,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
    }

    public func sentinelReplicas(masterName: String): ParameterizedRedisCommand<ArrayList<HashMap<String, String>>> {
        let commandArgs = CommandArgs().add(SentinelKeyWord.REPLICAS).add(masterName)
        return ParameterizedRedisCommand<ArrayList<HashMap<String, String>>>(
            CommandType.SENTINEL,
            commandArgs,
            ListOfMapResponseBuilder.INSTANCE
        )
    }

    public func sentinelReset(pattern: String): ParameterizedRedisCommand<Float64> {
        let commandArgs = CommandArgs().add(SentinelKeyWord.RESET).add(pattern)
        return ParameterizedRedisCommand<Float64>(
            CommandType.SENTINEL,
            commandArgs,
            ResponseBuilderFactory.doubleBuilder
        )
    }

    public func sentinelSentinels(masterName: String): ParameterizedRedisCommand<ArrayList<HashMap<String, String>>> {
        let commandArgs = CommandArgs().add(SentinelKeyWord.SENTINELS).add(masterName)
        return ParameterizedRedisCommand<ArrayList<HashMap<String, String>>>(
            CommandType.SENTINEL,
            commandArgs,
            ListOfMapResponseBuilder.INSTANCE
        )
    }

    public func sentinelSet(masterName: String, parameterMap: Map<String, String>): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(SentinelKeyWord.SET).add(masterName)
        for ((k, v) in parameterMap) {
            commandArgs.add(k).add(v)
        }
        return ParameterizedRedisCommand<String>(
            CommandType.SENTINEL,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
    }
}

public class ListOfMapResponseBuilder <: ResponseBuilder<ArrayList<HashMap<String, String>>> {
    public static let INSTANCE = ListOfMapResponseBuilder()

    public func build(message: RedisMessage): ArrayList<HashMap<String, String>> {
        if (let Some(message) <- message as ArraysRedisMessage) {
            let results = ArrayList<HashMap<String, String>>()
            if (message.isNull() || message.getSize() == 0) {
                return results
            }
            let children = message.getValue()

            if (let Some(children) <- children) {
                for (child in children) {
                    results.append(ResponseBuilderFactory.stringToStringMapBuilder.build(child))
                }
            }
            return results
        } else if (let Some(message) <- message as ErrorRedisMessage) {
            throw RedisException(message.getValue())
        }
        throw RedisException("Unsupported message type: ${message.getType()}")
    }
}
