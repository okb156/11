/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis.client.commands.impl

public class ModuleCommandsBuilderImpl <: ModuleCommandsBuilder {
    public func moduleLoad(path: String): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(CommandKeyword.LOAD).add(path)
        let command = ParameterizedRedisCommand<String>(
            CommandType.MODULE,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func moduleLoad(path: String, args: Array<String>): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(CommandKeyword.LOAD).add(path)
        for (arg in args) {
            commandArgs.add(arg)
        }
        let command = ParameterizedRedisCommand<String>(
            CommandType.MODULE,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func moduleLoadEx(path: String, params: ModuleLoadExParams): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(CommandKeyword.LOADEX)
        params.buildArgs(commandArgs)
        let command = ParameterizedRedisCommand<String>(
            CommandType.MODULE,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func moduleUnload(name: String): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(CommandKeyword.UNLOAD).add(name)
        let command = ParameterizedRedisCommand<String>(
            CommandType.MODULE,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func moduleList(): ParameterizedRedisCommand<ArrayList<Module>> {
        let commandArgs = CommandArgs().add(CommandKeyword.LIST)
        let command = ParameterizedRedisCommand<ArrayList<Module>>(
            CommandType.MODULE,
            commandArgs,
            ModuleListResponseBuilder()
        )
        return command
    }
}

/**
 *  解析MODULE LIST命令的响应
 */
public class ModuleListResponseBuilder <: ResponseBuilder<ArrayList<Module>> {
    public func build(message: RedisMessage): ArrayList<Module> {
        if (let Some(message) <- message as ArraysRedisMessage) {
            let results = ArrayList<Module>()
            if (message.isNull() || message.getSize() == 0) {
                return results
            }

            let children = message.getValue()
            if (let Some(children) <- children) {
                for (child in children) {
                    processChild(child, results)
                }
            }

            return results
        } else if (let Some(message) <- message as ErrorRedisMessage) {
            throw RedisException(message.getValue())
        }

        throw RedisException("Unsupported message type: ${message.getType()}")
    }

    private func processChild(message: RedisMessage, results: ArrayList<Module>) {
        let keyValues = ResponseBuilderFactory.listOfKeyValueBuilder.build(message)
        let attr = HashMap<String, Any>()
        for (keyValue in keyValues) {
            let titleMessage = keyValue.getKey()
            let contentMessage = keyValue.getValue()
            let title = ResponseBuilderFactory.stringBuilder.build(titleMessage)
            buildContents(title, contentMessage, attr)
        }
        let module = Module(attr)
        results.append(module)
    }

    private func buildContents(title: String, contentMessage: RedisMessage, moduleAttr: HashMap<String, Any>): Unit {
        match (title) {
            case "name" =>
                let name = ResponseBuilderFactory.stringBuilder.build(contentMessage)
                moduleAttr.put(title, name)
            case "ver" =>
                let version = ResponseBuilderFactory.integerBuilder.build(contentMessage)
                moduleAttr.put(title, version)
            case "path" =>
                let path = ResponseBuilderFactory.stringBuilder.build(contentMessage)
                moduleAttr.put(title, path)
            case "args" =>
                let args = ResponseBuilderFactory.listOfStringBuilder.build(contentMessage)
                moduleAttr.put(title, args)
            case _ => { => } // ignore
        }
    }
}
