/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis.client.commands.impl

/**
 * String类型相关操作
 */
public class StringCommandsImpl <: BaseCommand & StringCommands {
    public init(commandExecutor: CommandExecutor) {
        super(commandExecutor)
    }

    public func set(key: String, value: String): String {
        let redisCommand = RedisCommandBuilder.set(key, value)
        return executeCommand<String>(redisCommand)
    }

    public func set(key: String, value: String, params: SetParams): ?String {
        let redisCommand = RedisCommandBuilder.set(key, value, params)
        return executeCommand<String>(redisCommand)
    }

    public func get(key: String): ?String {
        let redisCommand = RedisCommandBuilder.get(key)
        return executeCommand<String>(redisCommand)
    }

    public func setGet(key: String, value: String): ?String {
        let redisCommand = RedisCommandBuilder.setGet(key, value)
        return executeCommand<String>(redisCommand)
    }

    public func setGet(key: String, value: String, params: SetParams): ?String {
        let redisCommand = RedisCommandBuilder.setGet(key, value, params)
        return executeCommand<String>(redisCommand)
    }

    public func getDel(key: String): ?String {
        let redisCommand = RedisCommandBuilder.getDel(key)
        return executeCommand<String>(redisCommand)
    }

    public func getEx(key: String, params: GetExParams): ?String {
        let redisCommand = RedisCommandBuilder.getEx(key, params)
        return executeCommand<String>(redisCommand)
    }

    public func setrange(key: String, offset: Int64, value: String): Int64 {
        let redisCommand = RedisCommandBuilder.setrange(key, offset, value)
        return executeCommand<Int64>(redisCommand)
    }

    public func getrange(key: String, startOffset: Int64, endOffset: Int64): ?String {
        let redisCommand = RedisCommandBuilder.getrange(key, startOffset, endOffset)
        return executeCommand<String>(redisCommand)
    }

    public func getSet(key: String, value: String): ?String {
        let redisCommand = RedisCommandBuilder.getSet(key, value)
        return executeCommand<String>(redisCommand)
    }

    public func setnx(key: String, value: String): Int64 {
        let redisCommand = RedisCommandBuilder.setnx(key, value)
        return executeCommand<Int64>(redisCommand)
    }

    public func setex(key: String, seconds: Int64, value: String): String {
        let redisCommand = RedisCommandBuilder.setex(key, seconds, value)
        return executeCommand<String>(redisCommand)
    }

    public func psetex(key: String, milliseconds: Int64, value: String): String {
        let redisCommand = RedisCommandBuilder.psetex(key, milliseconds, value)
        return executeCommand<String>(redisCommand)
    }

    public func mget(keys: Array<String>): Array<?String> {
        let redisCommand = RedisCommandBuilder.mget(keys)
        return executeCommand<Array<?String>>(redisCommand)
    }

    public func mset(keysvalues: Array<String>): String {
        let redisCommand = RedisCommandBuilder.mset(keysvalues)
        return executeCommand<String>(redisCommand)
    }

    public func msetnx(keysvalues: Array<String>): Int64 {
        let redisCommand = RedisCommandBuilder.msetnx(keysvalues)
        return executeCommand<Int64>(redisCommand)
    }

    public func incr(key: String): Int64 {
        let redisCommand = RedisCommandBuilder.incr(key)
        return executeCommand<Int64>(redisCommand)
    }

    public func incrBy(key: String, increment: Int64): Int64 {
        let redisCommand = RedisCommandBuilder.incrBy(key, increment)
        return executeCommand<Int64>(redisCommand)
    }

    public func incrByFloat(key: String, increment: Float64): Float64 {
        let redisCommand = RedisCommandBuilder.incrByFloat(key, increment)
        return executeCommand<Float64>(redisCommand)
    }

    public func decr(key: String): Int64 {
        let redisCommand = RedisCommandBuilder.decr(key)
        return executeCommand<Int64>(redisCommand)
    }

    public func decrBy(key: String, decrement: Int64): Int64 {
        let redisCommand = RedisCommandBuilder.decrBy(key, decrement)
        return executeCommand<Int64>(redisCommand)
    }

    public func append(key: String, value: String): Int64 {
        let redisCommand = RedisCommandBuilder.append(key, value)
        return executeCommand<Int64>(redisCommand)
    }

    public func substr(key: String, start: Int64, end: Int64): ?String {
        let redisCommand = RedisCommandBuilder.substr(key, start, end)
        return executeCommand<String>(redisCommand)
    }

    public func strlen(key: String): Int64 {
        let redisCommand = RedisCommandBuilder.strlen(key)
        return executeCommand<Int64>(redisCommand)
    }
}
