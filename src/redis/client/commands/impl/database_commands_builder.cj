/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis.client.commands.impl

public class DataBaseCommandsBuilderImpl <: DataBaseCommandsBuilder {
    public func select(index: Int64): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(index)
        let command = ParameterizedRedisCommand<String>(
            CommandType.SELECT,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func dbSize(): ParameterizedRedisCommand<Int64> {
        let command = ParameterizedRedisCommand<Int64>(CommandType.DBSIZE, ResponseBuilderFactory.integerBuilder)
        return command
    }

    public func flushDB(): ParameterizedRedisCommand<String> {
        let command = ParameterizedRedisCommand<String>(CommandType.FLUSHDB, ResponseBuilderFactory.stringBuilder)
        return command
    }

    public func flushDB(flushMode: FlushMode): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(flushMode.getBytes())
        let command = ParameterizedRedisCommand<String>(
            CommandType.FLUSHDB,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func flushAll(): ParameterizedRedisCommand<String> {
        let command = ParameterizedRedisCommand<String>(CommandType.FLUSHALL, ResponseBuilderFactory.stringBuilder)
        return command
    }

    public func flushAll(flushMode: FlushMode): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(flushMode.getBytes())
        let command = ParameterizedRedisCommand<String>(
            CommandType.FLUSHALL,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func swapDB(index1: Int64, index2: Int64): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(index1).add(index2)
        let command = ParameterizedRedisCommand<String>(
            CommandType.SWAPDB,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func copy(srcKey: String, dstKey: String, db: Int64, replace: Bool): ParameterizedRedisCommand<Bool> {
        let commandArgs = CommandArgs().key(srcKey).key(dstKey).add(CommandKeyword.DB).add(db)
        if (replace) {
            commandArgs.add(CommandKeyword.REPLACE)
        }

        let command = ParameterizedRedisCommand<Bool>(CommandType.COPY, commandArgs, ResponseBuilderFactory.boolBuilder)
        return command
    }

    public func move(key: String, dbIndex: Int64): ParameterizedRedisCommand<Int64> {
        let commandArgs = CommandArgs().key(key).add(dbIndex)
        let command = ParameterizedRedisCommand<Int64>(
            CommandType.MOVE,
            commandArgs,
            ResponseBuilderFactory.integerBuilder
        )
        return command
    }

    public func migrate(host: String, port: Int64, key: String, destinationDB: Int64, timeout: Int64): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(host).add(port).key(key).add(IntegerArg(destinationDB)).add(timeout)
        let command = ParameterizedRedisCommand<String>(
            CommandType.MIGRATE,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }

    public func migrate(
        host: String,
        port: Int64,
        destinationDB: Int64,
        timeout: Int64,
        params: MigrateParams,
        keys: Array<String>
    ): ParameterizedRedisCommand<String> {
        let commandArgs = CommandArgs().add(host).add(port).add(Array<Byte>()).add(destinationDB).add(timeout)
        params.buildArgs(commandArgs)

        commandArgs.add(CommandKeyword.KEYS)
        for (key in keys) {
            commandArgs.key(key)
        }

        let command = ParameterizedRedisCommand<String>(
            CommandType.MIGRATE,
            commandArgs,
            ResponseBuilderFactory.stringBuilder
        )
        return command
    }
}
