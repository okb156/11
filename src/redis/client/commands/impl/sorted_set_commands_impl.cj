/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis.client.commands.impl

public class SortedSetCommandsImpl <: BaseCommand & SortedSetCommands {
    public init(commandExecutor: CommandExecutor) {
        super(commandExecutor)
    }

    public func bzmpop(timeout: Int64, args: SortedSetOption, keys: Array<String>): ?(String, (String, Float64)) {
        let command = RedisCommandBuilder.bzmpop(timeout, args, keys)
        return executeCommand<(String, (String, Float64))>(command)
    }

    public func bzmpop(timeout: Int64, args: SortedSetOption, count: Int64, keys: Array<String>): ?(String, ArrayList<(String, 
        Float64)>) {
        let command = RedisCommandBuilder.bzmpop(timeout, args, count, keys)
        return executeCommand<(String, ArrayList<(String, Float64)>)>(command)
    }

    public func zadd(key: String, score: Float64, member: String): ?Int64 {
        let command = RedisCommandBuilder.zadd(key, score, member)
        return executeCommand<Int64>(command)
    }

    public func zadd(key: String, score: Float64, member: String, params: ZAddParams): ?Int64 {
        let command = RedisCommandBuilder.zadd(key, score, member, params)
        return executeCommand<Int64>(command)
    }

    public func zadd(key: String, scoreMembers: Map<String, Float64>): ?Int64 {
        let command = RedisCommandBuilder.zadd(key, scoreMembers)
        return executeCommand<Int64>(command)
    }

    public func zadd(key: String, scoreMembers: Map<String, Float64>, params: ZAddParams): ?Int64 {
        let command = RedisCommandBuilder.zadd(key, scoreMembers, params)
        return executeCommand<Int64>(command)
    }

    public func zaddIncr(key: String, score: Float64, member: String, params: ZAddParams): ?Float64 {
        let command = RedisCommandBuilder.zaddIncr(key, score, member, params)
        return executeCommand<Float64>(command)
    }

    public func zscore(key: String, member: String): ?Float64 {
        let command = RedisCommandBuilder.zscore(key, member)
        return executeCommand<Float64>(command)
    }

    public func bzpopmax(timeout: Int64, keys: Array<String>): ?(String, (String, Float64)) {
        let command = RedisCommandBuilder.bzpopmax(timeout, keys)
        return executeCommand<(String, (String, Float64))>(command)
    }

    public func bzpopmin(timeout: Int64, keys: Array<String>): ?(String, (String, Float64)) {
        let command = RedisCommandBuilder.bzpopmin(timeout, keys)
        return executeCommand<(String, (String, Float64))>(command)
    }

    public func zcard(key: String): Int64 {
        let command = RedisCommandBuilder.zcard(key)
        return executeCommand<Int64>(command)
    }

    public func zcount(key: String, min: Float64, max: Float64): Int64 {
        let command = RedisCommandBuilder.zcount(key, min, max)
        return executeCommand<Int64>(command)
    }

    public func zcount(key: String, min: String, max: String): Int64 {
        let command = RedisCommandBuilder.zcount(key, min, max)
        return executeCommand<Int64>(command)
    }

    public func zdiff(keys: Array<String>): ArrayList<String> {
        let command = RedisCommandBuilder.zdiff(keys)
        return executeCommand<ArrayList<String>>(command)
    }

    public func zdiffWithScores(keys: Array<String>): ArrayList<(String, Float64)> {
        let command = RedisCommandBuilder.zdiffWithScores(keys)
        return executeCommand<ArrayList<(String, Float64)>>(command)
    }

    public func zdiffstore(dstKey: String, keys: Array<String>): Int64 {
        let command = RedisCommandBuilder.zdiffstore(dstKey, keys)
        return executeCommand<Int64>(command)
    }

    public func zrange(key: String, params: ZRangeParams): ArrayList<String> {
        let command = RedisCommandBuilder.zrange(key, params)
        return executeCommand<ArrayList<String>>(command)
    }

    public func zrange(key: String, start: Int64, stop: Int64): ArrayList<String> {
        let command = RedisCommandBuilder.zrange(key, start, stop)
        return executeCommand<ArrayList<String>>(command)
    }

    public func zrangeWithScores(key: String, start: Int64, stop: Int64): ArrayList<(String, Float64)> {
        let command = RedisCommandBuilder.zrangeWithScores(key, start, stop)
        return executeCommand<ArrayList<(String, Float64)>>(command)
    }

    public func zrangeWithScores(key: String, params: ZRangeParams): ArrayList<(String, Float64)> {
        let command = RedisCommandBuilder.zrangeWithScores(key, params)
        return executeCommand<ArrayList<(String, Float64)>>(command)
    }

    public func zincrby(key: String, increment: Float64, member: String): Float64 {
        let command = RedisCommandBuilder.zincrby(key, increment, member)
        return executeCommand<Float64>(command)
    }

    public func zincrby(key: String, increment: Float64, member: String, params: ZIncrByParams): ?Float64 {
        let command = RedisCommandBuilder.zincrby(key, increment, member, params)
        return executeCommand<Float64>(command)
    }

    public func zinter(params: ZParams, keys: Array<String>): ArrayList<String> {
        let command = RedisCommandBuilder.zinter(params, keys)
        return executeCommand<ArrayList<String>>(command)
    }

    public func zinterWithScores(params: ZParams, keys: Array<String>): ArrayList<(String, Float64)> {
        let command = RedisCommandBuilder.zinterWithScores(params, keys)
        return executeCommand<ArrayList<(String, Float64)>>(command)
    }

    public func zintercard(keys: Array<String>): Int64 {
        let command = RedisCommandBuilder.zintercard(keys)
        return executeCommand<Int64>(command)
    }

    public func zintercard(limit: Int64, keys: Array<String>): Int64 {
        let command = RedisCommandBuilder.zintercard(limit, keys)
        return executeCommand<Int64>(command)
    }

    public func zinterstore(dstKey: String, sets: Array<String>): Int64 {
        let command = RedisCommandBuilder.zinterstore(dstKey, sets)
        return executeCommand<Int64>(command)
    }

    public func zinterstore(dstKey: String, params: ZParams, sets: Array<String>): Int64 {
        let command = RedisCommandBuilder.zinterstore(dstKey, params, sets)
        return executeCommand<Int64>(command)
    }

    public func zlexcount(key: String, min: String, max: String): Int64 {
        let command = RedisCommandBuilder.zlexcount(key, min, max)
        return executeCommand<Int64>(command)
    }

    public func zmpop(args: SortedSetOption, keys: Array<String>): ?(String, (String, Float64)) {
        let command = RedisCommandBuilder.zmpop(args, keys)
        return executeCommand<(String, (String, Float64))>(command)
    }

    public func zmpop(args: SortedSetOption, count: Int64, keys: Array<String>): ?(String, ArrayList<(String, Float64)>) {
        let command = RedisCommandBuilder.zmpop(args, count, keys)
        return executeCommand<(String, ArrayList<(String, Float64)>)>(command)
    }

    public func zmscore(key: String, members: Array<String>): ArrayList<?Float64> {
        let command = RedisCommandBuilder.zmscore(key, members)
        return executeCommand<ArrayList<?Float64>>(command)
    }

    public func zpopmax(key: String): ?(String, Float64) {
        let command = RedisCommandBuilder.zpopmax(key)
        return executeCommand<(String, Float64)>(command)
    }

    public func zpopmax(key: String, count: Int64): ArrayList<(String, Float64)> {
        let command = RedisCommandBuilder.zpopmax(key, count)
        return executeCommand<ArrayList<(String, Float64)>>(command)
    }

    public func zpopmin(key: String): ?(String, Float64) {
        let command = RedisCommandBuilder.zpopmin(key)
        return executeCommand<(String, Float64)>(command)
    }

    public func zpopmin(key: String, count: Int64): ArrayList<(String, Float64)> {
        let command = RedisCommandBuilder.zpopmin(key, count)
        return executeCommand<ArrayList<(String, Float64)>>(command)
    }

    public func zrandmember(key: String): ?String {
        let command = RedisCommandBuilder.zrandmember(key)
        return executeCommand<String>(command)
    }

    public func zrandmember(key: String, count: Int64): ArrayList<String> {
        let command = RedisCommandBuilder.zrandmember(key, count)
        return executeCommand<ArrayList<String>>(command)
    }

    public func zrandmemberWithScores(key: String, count: Int64): ArrayList<(String, Float64)> {
        let command = RedisCommandBuilder.zrandmemberWithScores(key, count)
        return executeCommand<ArrayList<(String, Float64)>>(command)
    }

    public func zrangeByLex(key: String, min: String, max: String): ArrayList<String> {
        let command = RedisCommandBuilder.zrangeByLex(key, min, max)
        return executeCommand<ArrayList<String>>(command)
    }

    public func zrangeByLex(key: String, min: String, max: String, offset: Int64, count: Int64): ArrayList<String> {
        let command = RedisCommandBuilder.zrangeByLex(key, min, max, offset, count)
        return executeCommand<ArrayList<String>>(command)
    }

    public func zrangeByScore(key: String, min: String, max: String): ArrayList<String> {
        let command = RedisCommandBuilder.zrangeByScore(key, min, max)
        return executeCommand<ArrayList<String>>(command)
    }

    public func zrangeByScore(key: String, min: String, max: String, offset: Int64, count: Int64): ArrayList<String> {
        let command = RedisCommandBuilder.zrangeByScore(key, min, max, offset, count)
        return executeCommand<ArrayList<String>>(command)
    }

    public func zrangeByScore(key: String, min: Float64, max: Float64): ArrayList<String> {
        let command = RedisCommandBuilder.zrangeByScore(key, min, max)
        return executeCommand<ArrayList<String>>(command)
    }

    public func zrangeByScore(key: String, min: Float64, max: Float64, offset: Int64, count: Int64): ArrayList<String> {
        let command = RedisCommandBuilder.zrangeByScore(key, min, max, offset, count)
        return executeCommand<ArrayList<String>>(command)
    }

    public func zrangeByScoreWithScores(key: String, min: String, max: String): ArrayList<(String, Float64)> {
        let command = RedisCommandBuilder.zrangeByScoreWithScores(key, min, max)
        return executeCommand<ArrayList<(String, Float64)>>(command)
    }

    public func zrangeByScoreWithScores(key: String, min: String, max: String, offset: Int64, count: Int64): ArrayList<(String, 
        Float64)> {
        let command = RedisCommandBuilder.zrangeByScoreWithScores(key, min, max, offset, count)
        return executeCommand<ArrayList<(String, Float64)>>(command)
    }

    public func zrangeByScoreWithScores(key: String, min: Float64, max: Float64): ArrayList<(String, Float64)> {
        let command = RedisCommandBuilder.zrangeByScoreWithScores(key, min, max)
        return executeCommand<ArrayList<(String, Float64)>>(command)
    }

    public func zrangeByScoreWithScores(key: String, min: Float64, max: Float64, offset: Int64, count: Int64): ArrayList<(String, 
        Float64)> {
        let command = RedisCommandBuilder.zrangeByScoreWithScores(key, min, max, offset, count)
        return executeCommand<ArrayList<(String, Float64)>>(command)
    }

    public func zrangestore(dest: String, src: String, params: ZRangeParams): Int64 {
        let command = RedisCommandBuilder.zrangestore(dest, src, params)
        return executeCommand<Int64>(command)
    }

    public func zrank(key: String, member: String): ?Int64 {
        let command = RedisCommandBuilder.zrank(key, member)
        return executeCommand<Int64>(command)
    }

    public func zrankWithScore(key: String, member: String): ?(Int64, Float64) {
        let command = RedisCommandBuilder.zrankWithScore(key, member)
        return executeCommand<(Int64, Float64)>(command)
    }

    public func zrem(key: String, members: Array<String>): Int64 {
        let command = RedisCommandBuilder.zrem(key, members)
        return executeCommand<Int64>(command)
    }

    public func zremrangeByLex(key: String, min: String, max: String): Int64 {
        let command = RedisCommandBuilder.zremrangeByLex(key, min, max)
        return executeCommand<Int64>(command)
    }

    public func zremrangeByRank(key: String, start: Int64, stop: Int64): Int64 {
        let command = RedisCommandBuilder.zremrangeByRank(key, start, stop)
        return executeCommand<Int64>(command)
    }

    public func zremrangeByScore(key: String, min: Float64, max: Float64): Int64 {
        let command = RedisCommandBuilder.zremrangeByScore(key, min, max)
        return executeCommand<Int64>(command)
    }

    public func zremrangeByScore(key: String, min: String, max: String): Int64 {
        let command = RedisCommandBuilder.zremrangeByScore(key, min, max)
        return executeCommand<Int64>(command)
    }

    public func zrevrange(key: String, start: Int64, stop: Int64): ArrayList<String> {
        let command = RedisCommandBuilder.zrevrange(key, start, stop)
        return executeCommand<ArrayList<String>>(command)
    }

    public func zrevrangeWithScores(key: String, start: Int64, stop: Int64): ArrayList<(String, Float64)> {
        let command = RedisCommandBuilder.zrevrangeWithScores(key, start, stop)
        return executeCommand<ArrayList<(String, Float64)>>(command)
    }

    public func zrevrangeByLex(key: String, min: String, max: String): ArrayList<String> {
        let command = RedisCommandBuilder.zrevrangeByLex(key, min, max)
        return executeCommand<ArrayList<String>>(command)
    }

    public func zrevrangeByLex(key: String, min: String, max: String, offset: Int64, count: Int64): ArrayList<String> {
        let command = RedisCommandBuilder.zrevrangeByLex(key, min, max, offset, count)
        return executeCommand<ArrayList<String>>(command)
    }

    public func zrevrangeByScore(key: String, min: String, max: String): ArrayList<String> {
        let command = RedisCommandBuilder.zrevrangeByScore(key, min, max)
        return executeCommand<ArrayList<String>>(command)
    }

    public func zrevrangeByScore(key: String, min: String, max: String, offset: Int64, count: Int64): ArrayList<String> {
        let command = RedisCommandBuilder.zrevrangeByScore(key, min, max, offset, count)
        return executeCommand<ArrayList<String>>(command)
    }

    public func zrevrangeByScore(key: String, min: Float64, max: Float64): ArrayList<String> {
        let command = RedisCommandBuilder.zrevrangeByScore(key, min, max)
        return executeCommand<ArrayList<String>>(command)
    }

    public func zrevrangeByScore(key: String, min: Float64, max: Float64, offset: Int64, count: Int64): ArrayList<String> {
        let command = RedisCommandBuilder.zrevrangeByScore(key, min, max, offset, count)
        return executeCommand<ArrayList<String>>(command)
    }

    public func zrevrangeByScoreWithScores(key: String, min: String, max: String): ArrayList<(String, Float64)> {
        let command = RedisCommandBuilder.zrevrangeByScoreWithScores(key, min, max)
        return executeCommand<ArrayList<(String, Float64)>>(command)
    }

    public func zrevrangeByScoreWithScores(key: String, min: String, max: String, offset: Int64, count: Int64): ArrayList<(String, 
        Float64)> {
        let command = RedisCommandBuilder.zrevrangeByScoreWithScores(key, min, max, offset, count)
        return executeCommand<ArrayList<(String, Float64)>>(command)
    }

    public func zrevrangeByScoreWithScores(key: String, min: Float64, max: Float64): ArrayList<(String, Float64)> {
        let command = RedisCommandBuilder.zrevrangeByScoreWithScores(key, min, max)
        return executeCommand<ArrayList<(String, Float64)>>(command)
    }

    public func zrevrangeByScoreWithScores(key: String, min: Float64, max: Float64, offset: Int64, count: Int64): ArrayList<(String, 
        Float64)> {
        let command = RedisCommandBuilder.zrevrangeByScoreWithScores(key, min, max, offset, count)
        return executeCommand<ArrayList<(String, Float64)>>(command)
    }

    public func zrevrank(key: String, member: String): ?Int64 {
        let command = RedisCommandBuilder.zrevrank(key, member)
        return executeCommand<Int64>(command)
    }

    public func zrevrankWithScore(key: String, member: String): ?(Int64, Float64) {
        let command = RedisCommandBuilder.zrevrankWithScore(key, member)
        return executeCommand<(Int64, Float64)>(command)
    }

    public func zscan(key: String, cursor: String): (String, ArrayList<(String, Float64)>) {
        let command = RedisCommandBuilder.zscan(key, cursor)
        return executeCommand<(String, ArrayList<(String, Float64)>)>(command)
    }

    public func zscan(key: String, cursor: String, params: ScanParams): (String, ArrayList<(String, Float64)>) {
        let command = RedisCommandBuilder.zscan(key, cursor, params)
        return executeCommand<(String, ArrayList<(String, Float64)>)>(command)
    }

    public func zunion(params: ZParams, keys: Array<String>): ArrayList<String> {
        let command = RedisCommandBuilder.zunion(params, keys)
        return executeCommand<ArrayList<String>>(command)
    }

    public func zunionWithScores(params: ZParams, keys: Array<String>): ArrayList<(String, Float64)> {
        let command = RedisCommandBuilder.zunionWithScores(params, keys)
        return executeCommand<ArrayList<(String, Float64)>>(command)
    }

    public func zunionstore(dstKey: String, keys: Array<String>): Int64 {
        let command = RedisCommandBuilder.zunionstore(dstKey, keys)
        return executeCommand<Int64>(command)
    }

    public func zunionstore(dstKey: String, params: ZParams, keys: Array<String>): Int64 {
        let command = RedisCommandBuilder.zunionstore(dstKey, params, keys)
        return executeCommand<Int64>(command)
    }
}
