/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis.client.api

/**
 * Shard Subscriber实现
 */
public interface ShardSubscriber {
    func setShardSubscriberListener(listener: ShardSubscriberListener): Unit

    func hasSubscribedChannels(): Bool

    func getSubscribedChannelsSize(): Int64

    func ssubscribe(channels: Array<String>): Unit

    func sunsubscribe(): Unit

    func sunsubscribe(channels: Array<String>): Unit

    func ping(): Unit

    func ping(argument: String): Unit
}

/**
 * 处理Shard订阅消息的Listener
 */
public interface ShardSubscriberListener {
    func onSMessage(channel: String, message: String): Unit

    func onSSubscribe(channel: String, subscribedChannels: Int64): Unit

    func onSUnsubscribe(channel: String, subscribedChannels: Int64): Unit

    func onPong(message: String): Unit

    func onExceptionCaught(ex: Exception): Unit
}
