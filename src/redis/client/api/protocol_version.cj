/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis.client.api

public enum ProtocolVersion <: Hashable & Equatable<ProtocolVersion> {
    RESP2 | RESP3

    public static func fromInteger(value: Int64) {
        match (value) {
            case 2 => return RESP2
            case 3 => return RESP3
            case _ => throw RedisException("Invalid RESP protocol: ${value}")
        }
    }

    public func getValue() {
        match (this) {
            case RESP2 => 2
            case RESP3 => 3
        }
    }

    @OverflowWrapping
    public override func hashCode(): Int64 {
        return getValue() * 37
    }

    public operator override func ==(other: ProtocolVersion): Bool {
        return this.getValue() == other.getValue()
    }

    public operator override func !=(other: ProtocolVersion): Bool {
        return this.getValue() != other.getValue()
    }
}
