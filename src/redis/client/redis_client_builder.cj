/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

package redis.client

public class RedisClientBuilder <: RedisClientBuilderBase {
    private static let logger: Logger = LoggerFactory.getLogger("redis-sdk")

    public static func builder(): RedisClientBuilder {
        return RedisClientBuilder()
    }

    public static func builder(clientConfig: RedisClientConfig): RedisClientBuilder {
        return RedisClientBuilder(clientConfig)
    }

    public static func buildRedisClient(host: String, port: UInt16): RedisClient {
        let clientBuilder = builder()
        return clientBuilder.host(host).port(port).build()
    }

    public static func buildRedisClient(host: String, port: UInt16, password: String): RedisClient {
        let clientBuilder = builder()
        return clientBuilder.host(host).port(port).password(password).build()
    }

    public static func buildRedisClient(host: String, port: UInt16, user: String, password: String): RedisClient {
        let clientBuilder = builder()
        return clientBuilder.host(host).port(port).user(user).password(password).build()
    }

    private let redisClientConfig: RedisClientConfig

    /**
     * Private Constructor
     */
    private init() {
        this(RedisClientConfig())
    }

    /**
     * Private Constructor
     */
    private init(clientConfig: RedisClientConfig) {
        super(clientConfig)
        this.redisClientConfig = clientConfig
    }

    public func host(host: String) {
        redisClientConfig.host = host
        return this
    }

    public func port(port: UInt16) {
        redisClientConfig.port = port
        return this
    }

    public func build(): RedisClient {
        let tcpEndpoint = buildClientEndPoint(this.redisClientConfig)
        configEndpoint(tcpEndpoint)

        let commandExecutor = DefaultCommandExecutor(tcpEndpoint)
        let redisClient = RedisClient(this.redisClientConfig, commandExecutor)

        let handshakeInfo = getHandShakeInfo(this.redisClientConfig)
        let clientInitializer: RedisClientInitializer = RedisClientInitializer(
            commandExecutor.getRespVersionAware(),
            handshakeInfo,
            redisClientConfig.waitResponseTimeout
        )
        tcpEndpoint.setConnectionInitializer(clientInitializer)

        tcpEndpoint.start()
        validClientEndPoint(tcpEndpoint)
        return redisClient
    }

    public static func configEndpoint(tcpEndpoint: ClientTcpEndpoint) {
        // RedisCodec
        let encoder = RedisCommandToByteEncoder()
        let decoder = ByteToRedisMessageDecoder()
        let redisCodec = RedisCodec(encoder, decoder)

        tcpEndpoint.setMessageCompletedHandler(decoder)
        tcpEndpoint.addFilter(redisCodec)
        tcpEndpoint.addFilter(RedisCommandHandler.getInstance())
    }

    public static func buildClientEndPoint(clientConfig: RedisClientConfig): ClientTcpEndpoint {
        let config = ClientEndpointConfig()
        if (let Some(host) <- clientConfig.host) {
            config.host = host
        } else {
            throw RedisException("The host of redis server is not provider")
        }

        if (let Some(port) <- clientConfig.port) {
            config.port = port
        } else {
            throw RedisException("The port of redis server is not provider")
        }

        clientConfig.configTcpEndpointOptions(config)

        let threadPool = buildThreadPool(clientConfig)
        let tcpEndpoint = ClientTcpEndpoint(config, threadPool)
        return tcpEndpoint
    }

    /**
     * 创建ThreadPool
     */
    public static func buildThreadPool(clientConfig: RedisClientConfigBase) {
        let threadPoolConfig = ThreadPoolConfig()
        threadPoolConfig.name = clientConfig.threadPoolName
        threadPoolConfig.queueSize = clientConfig.threadPoolQueueSize
        threadPoolConfig.maxThreads = clientConfig.maxThreads
        threadPoolConfig.minThreads = clientConfig.minThreads
        threadPoolConfig.threadIdleTimeout = clientConfig.threadIdleTimeout

        let threadPool = ThreadPoolFactory.createThreadPool(threadPoolConfig)
        return threadPool
    }

    public static func getHandShakeInfo(clientConfig: RedisClientConfigBase): ClientHandshakeInfo {
        let handshakeInfo = ClientHandshakeInfo()
        if (let Some(respVersion) <- clientConfig.respVersion) {
            handshakeInfo.respVersion = respVersion
        }

        if (let Some(user) <- clientConfig.user) {
            handshakeInfo.user = user
        }

        if (let Some(password) <- clientConfig.password) {
            handshakeInfo.password = password
        }

        if (let Some(clientName) <- clientConfig.clientName) {
            handshakeInfo.clientName = clientName
        }

        return handshakeInfo
    }

    /**
     * 验证ClientTcpEndpoint
     */
    public static func validClientEndPoint(endpoint: ClientTcpEndpoint) {
        try (session = endpoint.createSession()) {
            // 触发连接创建
        } catch (ex: Exception) {
            logger.error(ex.message, ex)
            throw RedisException(ex.message, ex)
        }
    }
}

public class RedisClientInitializer <: ConnectionInitializer {
    private let handshakeInfo: ClientHandshakeInfo

    private let respVersionAware: RespVersionAware

    private let waitResponseTimeout: Duration

    public init(respVersionAware: RespVersionAware, handshakeInfo: ClientHandshakeInfo, waitResponseTimeout: Duration) {
        this.respVersionAware = respVersionAware
        this.handshakeInfo = handshakeInfo
        this.waitResponseTimeout = waitResponseTimeout
    }

    /**
     * 处理协议对齐（Handshake）或者认证操作
     */
    public func initialize(session: Session): Unit {
        if (handshakeInfo.respVersion.isNone()) {
            // Only send auth command
            if (let Some(password) <- handshakeInfo.password) {
                let authCommandArgs = CommandArgs()
                if (let Some(user) <- handshakeInfo.user) {
                    authCommandArgs.add(StringArg(user))
                }

                authCommandArgs.add(BytesArg(password))

                let helloCommand = ParameterizedRedisCommand<String>(
                    CommandType.AUTH,
                    authCommandArgs,
                    ResponseBuilderFactory.stringBuilder
                )
                executeAuthCommand(session, helloCommand)
            }
            return
        }

        if (let Some(respVersion) <- handshakeInfo.respVersion) {
            if (handshakeInfo.password.isNone()) {
                // Only send hello command
                let handshakeCommandArgs = CommandArgs()
                handshakeCommandArgs.add(IntegerArg(respVersion.getValue()))
                let helloCommand = ParameterizedRedisCommand<RedisServerInfo>(
                    CommandType.HELLO,
                    handshakeCommandArgs,
                    HandshakeResponseBuilder()
                )
                let handshakeResult = executeHandshake(session, helloCommand)
                configRedisClient(handshakeResult)
                return
            }

            if (let Some(password) <- handshakeInfo.password) {
                if (handshakeInfo.user.isNone()) {
                    // send both auth command and hello command
                    let authCommandArgs = CommandArgs()
                    authCommandArgs.add(BytesArg(password))
                    let authCommand = ParameterizedRedisCommand<String>(
                        CommandType.AUTH,
                        authCommandArgs,
                        ResponseBuilderFactory.stringBuilder
                    )
                    executeAuthCommand(session, authCommand)

                    let handshakeCommandArgs = CommandArgs()
                    handshakeCommandArgs.add(IntegerArg(respVersion.getValue()))

                    if (let Some(clientName) <- handshakeInfo.clientName) {
                        handshakeCommandArgs.add(BytesArg(CommandKeyword.SETNAME))
                        handshakeCommandArgs.add(StringArg(clientName))
                    }

                    let helloCommand = ParameterizedRedisCommand<RedisServerInfo>(
                        CommandType.HELLO,
                        handshakeCommandArgs,
                        HandshakeResponseBuilder()
                    )
                    let handshakeResult = executeHandshake(session, helloCommand)
                    configRedisClient(handshakeResult)
                    return
                }

                // Only send hello command
                if (let Some(user) <- handshakeInfo.user) {
                    let handshakeCommandArgs = CommandArgs()
                    handshakeCommandArgs.add(IntegerArg(respVersion.getValue()))
                    handshakeCommandArgs.add(StringArg(user))
                    handshakeCommandArgs.add(BytesArg(password))

                    if (let Some(clientName) <- handshakeInfo.clientName) {
                        handshakeCommandArgs.add(BytesArg(CommandKeyword.SETNAME))
                        handshakeCommandArgs.add(StringArg(clientName))
                    }

                    let handshakeCommand = ParameterizedRedisCommand<RedisServerInfo>(
                        CommandType.HELLO,
                        handshakeCommandArgs,
                        HandshakeResponseBuilder()
                    )
                    let handshakeResult = executeHandshake(session, handshakeCommand)
                    configRedisClient(handshakeResult)
                }
            }
        }
    }

    private func executeHandshake(session: Session, command: ParameterizedRedisCommand<RedisServerInfo>): RedisServerInfo {
        session.writeAndFlushMessage(command)
        return command.buildResponse(waitResponseTimeout)
    }

    private func executeAuthCommand(session: Session, command: ParameterizedRedisCommand<String>): String {
        session.writeAndFlushMessage(command)
        return command.buildResponse(waitResponseTimeout)
    }

    private func configRedisClient(handshakeResult: RedisServerInfo): Unit {
        let protocol = handshakeResult.proto
        if (protocol != -1) {
            try {
                let respVersion = ProtocolVersion.fromInteger(protocol)
                respVersionAware.setRespVersion(respVersion)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }
}

public class RedisServerInfo {
    public let serverInfoMap = HashMap<String, Any>()

    public var server: String = ""
    public var version: String = ""
    public var proto: Int64 = -1
    public var id: Int64 = -1
    public var mode: String = ""
    public var role: String = ""
    public let modules = ArrayList<Module>()
}

public class HandshakeResponseBuilder <: ResponseBuilder<RedisServerInfo> {
    public func build(message: RedisMessage): RedisServerInfo {
        let keyValues = ResponseBuilderFactory.listOfKeyValueBuilder.build(message)
        let redisServerInfo = RedisServerInfo()
        for (keyValue in keyValues) {
            let titleMessage = keyValue.getKey()
            let contentMessage = keyValue.getValue()

            let title = ResponseBuilderFactory.stringBuilder.build(titleMessage)
            buildHandshakeResponse(title, contentMessage, redisServerInfo)
        }

        return redisServerInfo
    }

    /**
     * 解析HELLO命令的响应
     *
     */
    private func buildHandshakeResponse(
        title: String,
        valueMessage: RedisMessage,
        redisServerInfo: RedisServerInfo
    ): Unit {
        match (title) {
            case "server" =>
                let value = ResponseBuilderFactory.stringBuilder.build(valueMessage)
                redisServerInfo.server = value
                redisServerInfo.serverInfoMap.put(title, value)
            case "version" =>
                let value = ResponseBuilderFactory.stringBuilder.build(valueMessage)
                redisServerInfo.version = value
                redisServerInfo.serverInfoMap.put(title, value)
            case "proto" =>
                let value = ResponseBuilderFactory.integerBuilder.build(valueMessage)
                redisServerInfo.proto = value
                redisServerInfo.serverInfoMap.put(title, value)
            case "id" =>
                let value = ResponseBuilderFactory.integerBuilder.build(valueMessage)
                redisServerInfo.id = value
                redisServerInfo.serverInfoMap.put(title, value)
            case "mode" =>
                let value = ResponseBuilderFactory.stringBuilder.build(valueMessage)
                redisServerInfo.mode = value
                redisServerInfo.serverInfoMap.put(title, value)
            case "role" =>
                let value = ResponseBuilderFactory.stringBuilder.build(valueMessage)
                redisServerInfo.role = value
                redisServerInfo.serverInfoMap.put(title, value)
            case "modules" =>
                let modules = ModuleListResponseBuilder().build(valueMessage)
                if (!modules.isEmpty()) {
                    redisServerInfo.modules.appendAll(modules)
                }
                redisServerInfo.serverInfoMap.put(title, modules)
            case _ => { => } // 未来版本扩展的Key
        }
    }
}
