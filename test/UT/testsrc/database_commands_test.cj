/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

@Test
public class DatabaseCommandsTest {
    private var redisClient: ?RedisClient = None<RedisClient>

    protected func beforeAll(): Unit {
        redisClient = TestUtils.getRedisClient()
    }

    @TestCase
    public func testSelect(): Unit {
        let client = redisClient.getOrThrow()
        if (let Some(config) <- TestUtils.getRedisClientConfig() as ClusterRedisClientConfig) {
            @Assert(true)
            return
        }
        try {
            println("SELECT 1")
            var result = client.select(1)
            println(result)

            @Assert(result, "OK")

            println("SELECT 2")
            result = client.select(2)
            println(result)

            @Assert(result, "OK")
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        } finally {
            try {
                // 还原为默认数据库
                client.select(0)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    @TestCase
    public func testDbSize(): Unit {
        let client = redisClient.getOrThrow()
        if (let Some(config) <- TestUtils.getRedisClientConfig() as ClusterRedisClientConfig) {
            @Assert(true)
            return
        }
        let testDbSizeKey1 = "testDbSizeKey1"
        let testDbSizeValue1 = "testDbSizeValue1"

        let testDbSizeKey2 = "testDbSizeKey2"
        let testDbSizeValue2 = "testDbSizeValue2"
        try {
            println("SELECT 1")
            var result = client.select(1)
            println(result)
            @Assert(result, "OK")

            println("FLUSHDB")
            result = client.flushDB()
            println(result)
            @Assert(result, "OK")

            println("DBSIZE")
            var count = client.dbSize()
            println(count)
            @Assert(count, 0)

            println("SET ${testDbSizeKey1} ${testDbSizeValue1}")
            var setResult = client.set(testDbSizeKey1, testDbSizeValue1)
            println(setResult)
            println("DBSIZE")
            count = client.dbSize()
            println(count)
            @Assert(count, 1)

            println("SET ${testDbSizeKey2} ${testDbSizeValue2}")
            setResult = client.set(testDbSizeKey2, testDbSizeValue2)
            println(setResult)
            println("DBSIZE")
            count = client.dbSize()
            println(count)
            @Assert(count, 2)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        } finally {
            try {
                // 清空数据库
                client.flushDB()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            try {
                // 还原为默认数据库
                client.select(0)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    @TestCase
    public func testFlushDB(): Unit {
        let client = redisClient.getOrThrow()
        if (let Some(config) <- TestUtils.getRedisClientConfig() as ClusterRedisClientConfig) {
            @Assert(true)
            return
        }
        let testFlushDBKey1 = "testFlushDBKey1"
        let testFlushDBValue1 = "testFlushDBValue1"

        let testFlushDBKey2 = "testFlushDBKey2"
        let testFlushDBValue2 = "testFlushDBValue2"
        try {
            println("SELECT 1")
            var result = client.select(1)
            println(result)
            @Assert(result, "OK")

            println("FLUSHDB")
            result = client.flushDB()
            println(result)
            @Assert(result, "OK")

            println("DBSIZE")
            var count = client.dbSize()
            println(count)
            @Assert(count, 0)

            println("SET ${testFlushDBKey1} ${testFlushDBValue1}")
            var setResult = client.set(testFlushDBKey1, testFlushDBValue1)
            println(setResult)
            println("DBSIZE")
            count = client.dbSize()
            println(count)
            @Assert(count, 1)

            println("FLUSHDB SYNC")
            result = client.flushDB(FlushMode.SYNC)
            println(result)

            println("DBSIZE")
            count = client.dbSize()
            println(count)
            @Assert(count, 0)

            println("SET ${testFlushDBKey2} ${testFlushDBValue2}")
            setResult = client.set(testFlushDBKey2, testFlushDBValue2)
            println(setResult)

            println("FLUSHDB ASYNC")
            result = client.flushDB(FlushMode.ASYNC)
            println(result)

            println("DBSIZE")
            count = client.dbSize()
            println(count)
            @Assert(count, 0)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        } finally {
            try {
                // 清空数据库
                client.flushDB()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            try {
                // 还原为默认数据库
                client.select(0)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    @TestCase
    public func testFlushAll(): Unit {
        let client = redisClient.getOrThrow()
        if (let Some(config) <- TestUtils.getRedisClientConfig() as ClusterRedisClientConfig) {
            @Assert(true)
            return
        }
        let testFlushAllKey1 = "testFlushAllKey1"
        let testFlushAllValue1 = "testFlushAllValue1"

        let testFlushAllKey2 = "testFlushAllKey2"
        let testFlushAllValue2 = "testFlushAllValue2"
        try {
            println("SELECT 1")
            var result = client.select(1)
            println(result)

            println("SET ${testFlushAllKey1} ${testFlushAllValue1}")
            var setResult = client.set(testFlushAllKey1, testFlushAllValue1)
            println(setResult)

            println("SELECT 2")
            result = client.select(2)
            println(result)

            println("SET ${testFlushAllKey2} ${testFlushAllValue2}")
            setResult = client.set(testFlushAllKey2, testFlushAllValue2)
            println(setResult)

            println("FLUSHALL")
            result = client.flushAll()
            println(result)
            @Assert(result, "OK")

            println("DBSIZE")
            var count = client.dbSize()
            println(count)
            @Assert(count, 0)

            println("SELECT 1")
            result = client.select(1)
            println(result)

            println("DBSIZE")
            count = client.dbSize()
            println(count)
            @Assert(count, 0)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        } finally {
            try {
                // 清空数据库
                client.flushAll()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            try {
                // 还原为默认数据库
                client.select(0)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    @TestCase
    public func testFlushAllSync(): Unit {
        let client = redisClient.getOrThrow()
        if (let Some(config) <- TestUtils.getRedisClientConfig() as ClusterRedisClientConfig) {
            @Assert(true)
            return
        }
        let testFlushAllKey1 = "testFlushAllKey1"
        let testFlushAllValue1 = "testFlushAllValue1"

        let testFlushAllKey2 = "testFlushAllKey2"
        let testFlushAllValue2 = "testFlushAllValue2"
        try {
            println("SELECT 1")
            var result = client.select(1)
            println(result)

            println("SET ${testFlushAllKey1} ${testFlushAllValue1}")
            var setResult = client.set(testFlushAllKey1, testFlushAllValue1)
            println(setResult)

            println("SELECT 2")
            result = client.select(2)
            println(result)

            println("SET ${testFlushAllKey2} ${testFlushAllValue2}")
            setResult = client.set(testFlushAllKey2, testFlushAllValue2)
            println(setResult)

            println("FLUSHALL SYNC")
            result = client.flushAll(FlushMode.SYNC)
            println(result)
            @Assert(result, "OK")

            println("DBSIZE")
            var count = client.dbSize()
            println(count)
            @Assert(count, 0)

            println("SELECT 1")
            result = client.select(1)
            println(result)

            println("DBSIZE")
            count = client.dbSize()
            println(count)
            @Assert(count, 0)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        } finally {
            try {
                // 清空数据库
                client.flushAll()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            try {
                // 还原为默认数据库
                client.select(0)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    @TestCase
    public func testFlushAllASync(): Unit {
        let client = redisClient.getOrThrow()
        if (let Some(config) <- TestUtils.getRedisClientConfig() as ClusterRedisClientConfig) {
            @Assert(true)
            return
        }
        let testFlushAllKey1 = "testFlushAllKey1"
        let testFlushAllValue1 = "testFlushAllValue1"

        let testFlushAllKey2 = "testFlushAllKey2"
        let testFlushAllValue2 = "testFlushAllValue2"
        try {
            println("SELECT 1")
            var result = client.select(1)
            println(result)

            println("SET ${testFlushAllKey1} ${testFlushAllValue1}")
            var setResult = client.set(testFlushAllKey1, testFlushAllValue1)
            println(setResult)

            println("SELECT 2")
            result = client.select(2)
            println(result)

            println("SET ${testFlushAllKey2} ${testFlushAllValue2}")
            setResult = client.set(testFlushAllKey2, testFlushAllValue2)
            println(setResult)

            println("FLUSHALL")
            result = client.flushAll(FlushMode.ASYNC)
            println(result)
            @Assert(result, "OK")

            println("DBSIZE")
            var count = client.dbSize()
            println(count)
            @Assert(count, 0)

            println("SELECT 1")
            result = client.select(1)
            println(result)

            println("DBSIZE")
            count = client.dbSize()
            println(count)
            @Assert(count, 0)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        } finally {
            try {
                // 清空数据库
                client.flushAll()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            try {
                // 还原为默认数据库
                client.select(0)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    @TestCase
    public func testSwapDB(): Unit {
        let client = redisClient.getOrThrow()
        if (let Some(config) <- TestUtils.getRedisClientConfig() as ClusterRedisClientConfig) {
            @Assert(true)
            return
        }
        let testSwapDBKey1 = "testSwapDBKey1"
        let testSwapDBValue1 = "testSwapDBValue1"

        let testSwapDBKey2 = "testSwapDBKey2"
        let testSwapDBValue2 = "testSwapDBValue2"
        try {
            println("SELECT 1")
            var result = client.select(1)
            println(result)

            println("SET ${testSwapDBKey1} ${testSwapDBValue1}")
            var setResult = client.set(testSwapDBKey1, testSwapDBValue1)
            println(setResult)

            println("SELECT 2")
            result = client.select(2)
            println(result)

            println("SET ${testSwapDBKey2} ${testSwapDBValue2}")
            setResult = client.set(testSwapDBKey2, testSwapDBValue2)
            println(setResult)

            println("SWAPDB 1 2")
            result = client.swapDB(1, 2)
            println(result)
            @Assert(result, "OK")

            println("GET ${testSwapDBKey1}")
            var getResulst = client.get(testSwapDBKey1)
            println(getResulst)
            @Assert(getResulst, testSwapDBValue1)

            println("SELECT 1")
            result = client.select(1)
            println(result)

            println("GET ${testSwapDBKey2}")
            getResulst = client.get(testSwapDBKey2)
            println(getResulst)
            @Assert(getResulst, testSwapDBValue2)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        } finally {
            try {
                // 清空数据库
                client.flushAll()
            } catch (ex: Exception) {
                ex.printStackTrace()
            }

            try {
                // 还原为默认数据库
                client.select(0)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    @TestCase
    public func testCopy(): Unit {
        let client = redisClient.getOrThrow()
        if (let Some(config) <- TestUtils.getRedisClientConfig() as ClusterRedisClientConfig) {
            @Assert(true)
            return
        }

        let testCopyFromStringKey1 = "testCopyFromStringKey1"
        let testCopyFromStringValue1 = "testCopyFromStringValue1"
        let testCopyToStringKey2 = "testCopyToStringKey2"
        let testCopyNotExistKey = "testCopyNotExistKey"
        let copyToDbIndex = 1
        try {
            TestUtils.deleteKey(client, testCopyNotExistKey)

            println("COPY ${testCopyNotExistKey} ${testCopyToStringKey2}")
            var copyResult = client.copy(testCopyNotExistKey, testCopyToStringKey2, false)
            println(copyResult)
            @Assert(copyResult, false)

            println("SET ${testCopyFromStringKey1} ${testCopyFromStringValue1}")
            var status = client.set(testCopyFromStringKey1, testCopyFromStringValue1)
            println(status)

            println("COPY ${testCopyFromStringKey1} ${testCopyToStringKey2}")
            copyResult = client.copy(testCopyFromStringKey1, testCopyToStringKey2, false)
            println(copyResult)
            @Assert(copyResult, true)

            println("GET ${testCopyToStringKey2}")
            var getResult = client.get(testCopyToStringKey2)
            println(getResult)
            @Assert(getResult, testCopyFromStringValue1)

            println("COPY ${testCopyFromStringKey1} ${testCopyToStringKey2} REPLACE")
            copyResult = client.copy(testCopyFromStringKey1, testCopyToStringKey2, true)
            println(copyResult)

            println("GET ${testCopyToStringKey2}")
            getResult = client.get(testCopyToStringKey2)
            println(getResult)
            @Assert(getResult, testCopyFromStringValue1)

            println("COPY ${testCopyFromStringKey1} ${testCopyToStringKey2} DB ${copyToDbIndex}")
            copyResult = client.copy(testCopyFromStringKey1, testCopyToStringKey2, copyToDbIndex, false)
            println(copyResult)

            println("SELECT ${copyToDbIndex}")
            var selectResult = client.select(copyToDbIndex)
            println(selectResult)

            println("GET ${testCopyToStringKey2}")
            getResult = client.get(testCopyToStringKey2)
            println(getResult)
            @Assert(getResult, testCopyFromStringValue1)

            println("SELECT 0")
            selectResult = client.select(0)
            println(selectResult)

            println("COPY ${testCopyFromStringKey1} ${testCopyToStringKey2} DB ${copyToDbIndex} REPLACE")
            copyResult = client.copy(testCopyFromStringKey1, testCopyToStringKey2, copyToDbIndex, true)
            println(copyResult)

            println("SELECT ${copyToDbIndex}")
            selectResult = client.select(copyToDbIndex)
            println(selectResult)

            println("GET ${testCopyToStringKey2}")
            getResult = client.get(testCopyToStringKey2)
            println(getResult)
            @Assert(getResult, testCopyFromStringValue1)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        } finally {
            // 先删除copyToDbIndex对应的DB中的key
            try {
                println("SELECT ${copyToDbIndex}")
                let selectResult = client.select(copyToDbIndex)
                println(selectResult)
                TestUtils.deleteKey(client, testCopyToStringKey2)
            } catch (ex: Exception) {
                ex.printStackTrace()
            } finally {
                println("SELECT 0")
                let selectResult = client.select(0)
                println(selectResult)
            }

            TestUtils.deleteKey(client, testCopyFromStringKey1)
            TestUtils.deleteKey(client, testCopyToStringKey2)
        }
    }

    @TestCase
    public func testMove(): Unit {
        let client = redisClient.getOrThrow()
        if (let Some(config) <- TestUtils.getRedisClientConfig() as ClusterRedisClientConfig) {
            @Assert(true)
            return
        }

        let testMoveStringKey1 = "testMoveStringKey1"
        let testMoveStringValue1 = "testMoveStringValue1"
        let testMoveNotExistKey = "testMoveNotExistKey"
        let moveToDbIndex = 2
        try {
            TestUtils.deleteKey(client, testMoveNotExistKey)

            println("MOVE ${testMoveNotExistKey} ${moveToDbIndex}")
            var moveResult = client.move(testMoveNotExistKey, moveToDbIndex)
            println(moveResult)
            @Assert(moveResult, 0)

            println("SET ${testMoveStringKey1} ${testMoveStringValue1}")
            var status = client.set(testMoveStringKey1, testMoveStringValue1)
            println(status)

            println("MOVE ${testMoveStringKey1} ${moveToDbIndex}")
            moveResult = client.move(testMoveStringKey1, moveToDbIndex)
            println(moveResult)
            @Assert(moveResult, 1)

            println("GET ${testMoveStringKey1}")
            var getResult = client.get(testMoveStringKey1)
            println(getResult)
            @Assert(getResult, None)

            println("MOVE ${testMoveStringKey1} ${moveToDbIndex}")
            moveResult = client.move(testMoveStringKey1, moveToDbIndex)
            println(moveResult)
            @Assert(moveResult, 0)

            println("SELECT ${moveToDbIndex}")
            var selectResult = client.select(moveToDbIndex)
            println(selectResult)

            println("GET ${testMoveStringKey1}")
            getResult = client.get(testMoveStringKey1)
            println(getResult)
            @Assert(getResult, testMoveStringValue1)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        } finally {
            TestUtils.deleteKey(client, testMoveStringKey1)

            try {
                println("SELECT ${moveToDbIndex}")
                let selectResult = client.select(moveToDbIndex)
                println(selectResult)
            } catch (ex: Exception) {
                ex.printStackTrace()
            } finally {
                println("SELECT 0")
                let selectResult = client.select(0)
                println(selectResult)
            }
        }
    }
}
