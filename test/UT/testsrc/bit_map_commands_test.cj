/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

@Test
public class BitMapCommandsTest {
    private var redisClient: ?UnifiedRedisClient = None<UnifiedRedisClient>

    protected func beforeAll(): Unit {
        redisClient = TestUtils.getUnifiedRedisClient()
    }

    @TestCase
    public func testBitcount() {
        let client = redisClient.getOrThrow()

        let key1 = "foo"

        try {
            TestUtils.deleteKey(client, key1)

            println("SETBIT ${key1} 16 1")
            var res = client.setbit(key1, 16, true)
            println(res)

            println("SETBIT ${key1} 24 1")
            res = client.setbit(key1, 24, true)
            println(res)

            println("SETBIT ${key1} 40 1")
            res = client.setbit(key1, 40, true)
            println(res)

            println("SETBIT ${key1} 56 1")
            res = client.setbit(key1, 56, true)
            println(res)

            println("BITCOUNT ${key1}")
            var res1 = client.bitcount(key1)
            println(res1)
            @Assert(4,res1)

            println("BITCOUNT ${key1} 2 5")
            res1 = client.bitcount(key1, 2, 5)
            println(res1)
            @Assert(3,res1)

            println("BITCOUNT ${key1} 2 5 BYTE")
            res1 = client.bitcount(key1, 2, 5, BitCountOption.BYTE)
            println(res1)
            @Assert(3,res1)

            println("BITCOUNT ${key1} 2 5 BIT")
            res1 = client.bitcount(key1, 2, 5, BitCountOption.BIT)
            println(res1)
            @Assert(0,res1)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        } finally {
            TestUtils.deleteKey(client, key1)
        }
    }

    @TestCase
    public func testBitfield() {
        let client = redisClient.getOrThrow()

        let key1 = "foo"

        try {
            TestUtils.deleteKey(client, key1)

            println("BIFTIELD ${key1} INCRBY i5 100 1 GET u4 0")
            var res = client.bitfield(key1, "INCRBY", "i5", "100", "1", "GET", "u4", "0").getOrThrow()
            println(res)
            @Assert(1,res[0])
            @Assert(0,res[1])
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        } finally {
            TestUtils.deleteKey(client, key1)
        }
    }

    @TestCase
    public func testBitfieldReadonly() {
        let client = redisClient.getOrThrow()

        let key1 = "foo"

        try {
            TestUtils.deleteKey(client, key1)

            println("BIFTIELD ${key1} INCRBY i5 100 1 GET u4 0")
            var res = client.bitfield(key1, "INCRBY", "i5", "100", "1", "GET", "u4", "0").getOrThrow()
            println(res)
            @Assert(1,res[0])
            @Assert(0,res[1])

            println("BIFTIELD_RO ${key1} GET i5 100")
            res = client.bitfieldReadonly(key1, "GET", "i5", "100")
            println(res)
            @Assert(1,res[0])

            try {
                println("BIFTIELD_RO ${key1} INCRBY i5 100 1 GET u4 0")
                res = client.bitfieldReadonly(key1, "INCRBY", "i5", "100", "1", "GET", "u4", "0")
                println(res)
                @Assert(false)
            } catch (ex: Exception) {
            }
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        } finally {
            TestUtils.deleteKey(client, key1)
        }
    }

    @TestCase
    public func testBitop() {
        let client = redisClient.getOrThrow()

        let key1 = "{testBitop}foo"
        let key2 = "{testBitop}bar"
        let key3 = "{testBitop}car"

        try {
            TestUtils.deleteKey(client, key1)
            TestUtils.deleteKey(client, key2)
            TestUtils.deleteKey(client, key3)

            println("SET list1 `")
            var res = client.set(key1, "`")
            println(res)

            println("SET list1 D")
            res = client.set(key2, "D")
            println(res)

            println("BITOP AND ${key3} ${key1} ${key2}")
            var res1 = client.bitop(BitOP.AND, key3, key1, key2)
            println(res1)

            println("GET ${key3}")
            var res2 = client.get(key3)
            println(res2)
            @Assert("@",res2)

            println("BITOP OR ${key3} ${key1} ${key2}")
            res1 = client.bitop(BitOP.OR, key3, key1, key2)
            println(res1)

            println("GET ${key3}")
            res2 = client.get(key3)
            println(res2)
            @Assert("d",res2)

            println("BITOP XOR ${key3} ${key1} ${key2}")
            res1 = client.bitop(BitOP.XOR, key3, key1, key2)
            println(res1)

            println("GET ${key3}")
            res2 = client.get(key3)
            println(res2)
            @Assert("$",res2)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        } finally {
            TestUtils.deleteKey(client, key1)
            TestUtils.deleteKey(client, key2)
            TestUtils.deleteKey(client, key3)
        }
    }

    @TestCase
    public func testBitopNot() {
        let client = redisClient.getOrThrow()

        let key1 = "{testBitopNot}foo"
        let key2 = "{testBitopNot}bar"

        try {
            TestUtils.deleteKey(client, key1)
            TestUtils.deleteKey(client, key2)

            println("SETBIT ${key1} 0 1")
            var res = client.setbit(key1, 0, true)
            println(res)

            println("SETBIT ${key1} 4 1")
            res = client.setbit(key1, 4, true)
            println(res)

            println("BITOP NOT ${key2} ${key1} ")
            var res1 = client.bitop(BitOP.NOT, key2, key1)
            println(res1)

            println("GET ${key2}")
            var res2 = client.get(key2)
            println(res2)
            @Assert("w",res2)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        } finally {
            TestUtils.deleteKey(client, key1)
            TestUtils.deleteKey(client, key2)
        }
    }

    @TestCase
    public func testBitpos() {
        let client = redisClient.getOrThrow()

        let key1 = "foo"

        try {
            TestUtils.deleteKey(client, key1)

            println("SET ${key1} 0")
            var res = client.set(key1, "0")
            println(res)

            println("SETBIT ${key1} 3 1")
            var res1 = client.setbit(key1, 3, true)
            println(res1)

            println("SETBIT ${key1} 7 1")
            res1 = client.setbit(key1, 7, true)
            println(res1)

            println("SETBIT ${key1} 13 1")
            res1 = client.setbit(key1, 13, true)
            println(res1)

            println("SETBIT ${key1} 39 1")
            res1 = client.setbit(key1, 39, true)
            println(res1)

            println("BITPOS ${key1} 1")
            var res2 = client.bitpos(key1, true)
            println(res2)
            @Assert(2,res2)

            println("BITPOS ${key1} 0")
            res2 = client.bitpos(key1, false)
            println(res2)
            @Assert(0,res2)

            println("BITPOS ${key1} 1 1")
            res2 = client.bitpos(key1, true, BitPosParams(1))
            println(res2)
            @Assert(13,res2)

            println("BITPOS ${key1} 1 2 3")
            res2 = client.bitpos(key1, true, BitPosParams(2).end(3))
            println(res2)
            @Assert(-1,res2)

            println("BITPOS ${key1} 0 2 3")
            res2 = client.bitpos(key1, false, BitPosParams(2).end(3))
            println(res2)
            @Assert(16,res2)

            println("BITPOS ${key1} 1 3 4")
            res2 = client.bitpos(key1, true, BitPosParams(3).end(4))
            println(res2)
            @Assert(39,res2)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        } finally {
            TestUtils.deleteKey(client, key1)
        }
    }

    @TestCase
    public func testBitposModifier() {
        let client = redisClient.getOrThrow()

        let key1 = "foo"

        try {
            TestUtils.deleteKey(client, key1)

            println("SET ${key1} \\x00\\xff\\xf0")
            var res = client.set(key1, "\\x00\\xff\\xf0")
            println(res)

            println("BITPOS ${key1} 0")
            var res1 = client.bitpos(key1, false)
            println(res1)
            @Assert(0,res1)

            println("BITPOS ${key1} 1")
            res1 = client.bitpos(key1, true)
            println(res1)
            @Assert(1,res1)

            println("BITPOS ${key1} 1 2")
            res1 = client.bitpos(key1, true, BitPosParams(2))
            println(res1)
            @Assert(18,res1)

            println("BITPOS ${key1} 1 2 -1")
            res1 = client.bitpos(key1, true, BitPosParams(2).end(-1))
            println(res1)
            @Assert(18,res1)

            println("BITPOS ${key1} 1 2 -1 BYTE")
            res1 = client.bitpos(key1, true, BitPosParams(2).end(-1).byte())
            println(res1)
            @Assert(18,res1)

            println("BITPOS ${key1} 1 7 15 BIT")
            res1 = client.bitpos(key1, true, BitPosParams(7).end(15).bit())
            println(res1)
            @Assert(9,res1)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        } finally {
            TestUtils.deleteKey(client, key1)
        }
    }

    @TestCase
    public func testSetAndgetbit() {
        let client = redisClient.getOrThrow()

        let key1 = "foo"

        try {
            TestUtils.deleteKey(client, key1)

            println("SETBIT ${key1} 0 1")
            var res = client.setbit(key1, 0, true)
            println(res)
            @Assert(false,res)

            println("GETBIT ${key1} 0")
            res = client.getbit(key1, 0)
            println(res)
            @Assert(true,res)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        } finally {
            TestUtils.deleteKey(client, key1)
        }
    }
}
