/**
 * Copyright 2024 Beijing Baolande Software Corporation
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Runtime Library Exception to the Apache 2.0 License:
 *
 * As an exception, if you use this Software to compile your source code and
 * portions of this Software are embedded into the binary product as a result,
 * you may redistribute such product without providing attribution as would
 * otherwise be required by Sections 4(a), 4(b) and 4(d) of the License.
 */

from std import unittest.*
from std import unittest.testmacro.*
from std import collection.Set
from std import collection.HashSet
from std import collection.ArrayList
from std import convert.*
from std import math.*
from std import sort.*
from std import time.*
from redis_sdk import redis.client.api.*
from redis_sdk import redis.client.RedisClient
from redis_sdk_test_utils import utils.TestUtils

@Test
public class ControlCommandsTest {
    private var redisClient: ?RedisClient = None<RedisClient>

    protected func beforeAll(): Unit {
        redisClient = TestUtils.getRedisClient()
    }

    @TestCase
    public func testRole(): Unit {
        let client = redisClient.getOrThrow()
        try {
            println("ROLE")
            var replicationRole = client.role()
            println(replicationRole)

            @Assert(true)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testMemoryDoctor(): Unit {
        let client = redisClient.getOrThrow()
        try {
            println("MEMORY DOCTOR")
            var result = client.memoryDoctor()
            println(result)

            @Assert(true)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testMemoryUsage(): Unit {
        let client = redisClient.getOrThrow()
        let testMemoryUsageKey1 = "testMemoryUsageKey1"
        let testMemoryUsageValue1 = TestUtils.generateLengthedString(65536)
        try {
            println("SET ${testMemoryUsageKey1} 65536LengthedString.....")
            var result = client.set(testMemoryUsageKey1, testMemoryUsageValue1)
            println(result)

            println("MEMORY USAGE ${testMemoryUsageKey1}")
            var memoryUsage = client.memoryUsage(testMemoryUsageKey1)
            println(memoryUsage)

            @Assert(true)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        } finally {
            try {
                client.del(testMemoryUsageKey1)
            } catch (ex: Exception) {
                ex.printStackTrace()
            }
        }
    }

    @TestCase
    public func testMemoryPurge(): Unit {
        let client = redisClient.getOrThrow()
        try {
            println("MEMORY PURGE")
            var result = client.memoryPurge()
            println(result)

            @Assert(true)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }

    @TestCase
    public func testMemoryStats(): Unit {
        let client = redisClient.getOrThrow()
        try {
            println("MEMORY STATS")
            var result = client.memoryStats()
            println(result)

            @Assert(true)
        } catch (ex: Exception) {
            ex.printStackTrace()
            @Assert(false)
        }
    }
}
