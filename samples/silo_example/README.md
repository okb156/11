# 项目使用介绍

## 1 简介

提供Silo框架调用Redis客户端的示例

## 2 准备工作

1. 下载https://gitee.com/HW-PLLab/silo_binary.git工程

2. 解压[silo_demo_x86_2023-11-23.tar.gz](https://gitee.com/HW-PLLab/silo_binary/blob/master/silo_demo_x86_2023-11-23.tar.gz)，获取silo_demo

3. 下载https://gitee.com/HW-PLLab/redis-sdk.git工程

4. 执行cjpm build编译redis-sdk

5. 将编译产物build/release/redis_sdk build/release/hyperion拷贝到silo_demo的libs目录

6. silo_demo的module.json添加依赖

   ```json
   "package_requires": {
   		"path_option": [
   			......
   			"libs/redis_sdk",
   			"libs/hyperion"
   		],
   		"package_option": {}
   	},
   ```

7. 将本工程拷贝到silo_demo根目录

8. 按silo_demo的Readme执行初始化等工作

